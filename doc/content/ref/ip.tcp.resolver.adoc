+++
title = "ip.tcp.resolver"
+++

[source,lua]
----
local resolver = ip.tcp.resolver.new()
local flags = ip.resolver_flag;
local results = resolver:resolve(
    'www.example.com', '',
    bit.bor(flags.all_matching, flags.v4_mapped)
)
for _, e in pairs(results) do
    print(e.ep_addr)
    print(e.ep_port)
    print(e.host_name)
    print(e.service_name)
end
----

== Functions

=== `new() -> ip.tcp.resolver`

Constructor.

=== `resolve(self, host: string, service: string [, flags: number]) -> table`

Perform a forward resolution. Current fiber is suspended until operation
finishes.

`flags` is `0` or an or-combination of values from
link:../ip.resolver_flag/[`ip.resolver_flag`].

Returns a list of results. Each result will be a table with the following
members:

* `ep_addr: ip.address`.
* `ep_port: number`.
* `host_name: string`.
* `service_name: string`.

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__basic_resolver/async_resolve/overload3.html[More
info on Boost.Asio documentation].

=== `cancel(self)`

Cancel any operations that are waiting on the resolver.
