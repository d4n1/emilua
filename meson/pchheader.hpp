#include <condition_variable>
#include <system_error>
#include <string_view>
#include <filesystem>
#include <iostream>
#include <optional>
#include <fstream>
#include <variant>
#include <cstdio>
#include <atomic>
#include <memory>
#include <thread>
#include <deque>
#include <mutex>
#include <new>

#include <CLI/CLI.hpp>

#include <fmt/chrono.h>
#include <fmt/format.h>

#include <boost/outcome/basic_result.hpp>
#include <boost/outcome/policy/all_narrow.hpp>
#include <boost/outcome/policy/terminate.hpp>

#include <boost/core/ignore_unused.hpp>
#include <boost/intrusive/list.hpp>
#include <boost/asio.hpp>
#include <boost/hana.hpp>

#include <boost/beast/core/stream_traits.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>

#include <trial/protocol/buffer/string.hpp>
#include <trial/protocol/json/reader.hpp>
#include <trial/protocol/json/writer.hpp>

extern "C" {
#include <lauxlib.h>
#include <luajit.h>
#include <lualib.h>
#include <lua.h>
}
