= Emilua

:_:
:cpp: C++

An execution engine{_}footnote:[If you don't know what is an execution engine
think NodeJS.] for Lua.

Features:

* Complete fiber API (sync primitives, interruption API, clean-up handlers,
  fiber local storage, assert-like scheduling constraints, ...).
* Integrates with Lua builtins (i.e. you can mix up fibers and coroutines,
  modules, ...).
* HTTP{_}footnote:[WebSocket planned for next releases.].
* Actor API (each VM is an actor and you can spawn many of them to be scheduled
  through the same (or a new) thread pool).
* Native JSON module.
* Cross-platform (should run anywhere Boost.Asio also runs).

== Hello World

[source,lua]
----
local sleep_for = require('sleep_for')

local numbers = {8, 42, 38, 111, 2, 39, 1}

for _, n in pairs(numbers) do
    spawn(function()
        sleep_for(n * 10)
        print(n)
    end)
end
----

== Actor example

[source,lua]
----
local inbox = require('inbox')

print('actor started with _CONTEXT=' .. _CONTEXT)

if _CONTEXT == 'main' then
    local ch = spawn_vm('.')
    ch:send{ from = inbox, body = 'Hello World' }
    local m = inbox:recv()
    print(m)
else assert(_CONTEXT == 'worker')
    local m = inbox:recv()
    m.from:send(m.body)
end
----

== Dependencies

* Meson 0.55.0 or later.
* {cpp}17 compiler.
* LuaJIT.
* Boost.Asio (and other Boost libraries as well).
* `re2c`.
* `xxd`.

== Optional dependencies

* Curses: coloured output on errors.
* GNU AWK: tests.

== "Bundled" dependencies

Run:

[source,bash]
----
git submodule update --init
----

This will download the extra dependencies:

* fmt.
* CLI11.
* Trial.Protocol.

== Install

* https://aur.archlinux.org/packages/emilua/[ArchLinux].

== BUILD

[source,bash]
----
$ mkdir build
$ cd build
$ meson ..
$ ninja
----

An `emilua` binary will be generated. Copy it to your `$PATH`.

You can also pass options when you invoke `meson`. One of the options is `-D
enable_http=true`.

It's encouraged that you build luajit with `-DLUAJIT_ENABLE_LUA52COMPAT`.

== LICENSE

* C/{cpp} source code under Boost Software License.
* Lua code (including snippets scattered through the documentation) distributed
  in this repo is released as public domain.
* Submodules and dependencies have their own licensing terms.
* I haven't decided on the license for the documentation yet and I'll require
  copyright attribution (so I can change the license to my choosing) on any
  contribution here until I decide a good license.

== Documentation

You need to have `asciidoctor` and `hugo`. Then, just run inside the `doc`
directory:

[source,bash]
----
$ hugo server
----

== Tests

[source,bash]
----
$ ninja test
----

You also need to have GNU AWK installed to run the tests.

== ROADMAP

* Package manager.
* Native plug-in API.
** Regex plug-in.
** D-Bus plug-in.
** Python plug-in.
** Redis plug-in.
* Linux namespaces powered actors when available+requested.
* Self-contained executables.
* REPL.
* Debugger.
* Custom memory allocator per VM.
* Try to infect other languages with similar ideas.
